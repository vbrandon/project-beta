import React, { useEffect, useState } from 'react';
import { Link, NavLink } from 'react-router-dom';

function AutomobilesList(props) {
    const [automobiles, setAutomobiles] = useState([]);

    const getData = async () => {
        const automobilesUrl = "http://localhost:8100/api/automobiles/";
        const response = await fetch(automobilesUrl);

        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos)
        }
    }


    useEffect(() => {
        getData()
    }, [])

    return (
        <div className="container my-4">
            <h1 className="text-center mb-4">List of Automobiles</h1>
            <div className="text-center">
                <NavLink to="/automobiles/new" className="btn btn-primary">
                    Create New Automobile
                </NavLink>
            </div>
            <div className="row">
                {automobiles.map((automobile) => (
                    <div className="col-md-4 mb-4" key={automobile.href}>
                        <div className="card">
                            <Link to={`${automobile.id}`} style={{ textDecoration: 'none', color: 'black' }} key={automobile.id}>
                                <img
                                    src={automobile.model.picture_url}
                                    className="card-img-top"
                                    alt="Automobile"
                                    style={{ objectFit: "cover" }}
                                />
                            </Link>
                            <div className="card-body text-center">
                                <Link to={`${automobile.id}`} style={{ textDecoration: 'none', color: 'black' }} key={automobile.id}>
                                    <p className="card-text"> Automobile Name: {automobile.name}</p>
                                    <p className="card-text"> Automobile Year: {automobile.year}</p>
                                    <p className="card-text"> Automobile Color: {automobile.color}</p>
                                    <p className="card-text"> Automobile Vin: {automobile.vin}</p>
                                    <p className="card-text">Model: {automobile.model.name}</p>
                                    <p className="card-text"> Automobile Manufacturer: {automobile.model.manufacturer.name}</p>
                                </Link>
                            </div>
                        </div>

                    </div>
                ))}
            </div>
        </div>
    );
}

export default AutomobilesList;
