import React, { useState, useEffect } from 'react'

export default function SaleForm(props) {
    const [automobile, setAutomobile] = useState('')
    const [automobiles, setAutomobiles] = useState([])

    const [salesPerson, setSalesPerson] = useState('')
    const [salesPersons, setSalesPersons] = useState([])

    const [customer, setCustomer] = useState('')
    const [customers, setCustomers] = useState([])

    const [price, setPrice] = useState('')

    const handleAutomobileChange = (event) => {
        const value = event.target.value;
        setAutomobile(value);
    }
    const handleSalesPersonChange = (event) => {
        const value = event.target.value;
        setSalesPerson(value);
    }
    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }
    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    }




    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}

        data.price = parseInt(price)
        data.automobile = automobile
        data.salesPerson = parseInt(salesPerson)
        data.customer = parseInt(customer)
        const salesUrl = "http://localhost:8090/api/sales/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(salesUrl, fetchConfig)
        if (response.ok) {
            const newSales = await response.json()
            setPrice('')
            setAutomobile('')
            setSalesPerson('')
            setCustomer('')
            props.getSales()
        }
    }



        const fetchAutomobiles = async () => {
            const url = 'http://localhost:8100/api/automobiles/';

            const response = await fetch(url);

            if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos)
            }
        }





        const fetchSalesPersons = async () => {
            const url = 'http://localhost:8090/api/salesPersons/';

            const response = await fetch(url);

            if (response.ok) {
            const data = await response.json();
            setSalesPersons(data.salesPerson)
            }
        }


        const fetchCustomers = async () => {
            const url = 'http://localhost:8090/api/customers/';

            const response = await fetch(url);

            if (response.ok) {
            const data = await response.json();
            setCustomers(data.customer)
            }
        }
        useEffect(() => {
            fetchAutomobiles()
            fetchSalesPersons()
            fetchCustomers()
            }, []);





return (
    <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Record a new sale</h1>
            <form onSubmit={handleSubmit} id="create-sale-form">
                <div className="mb-3">
                    <select value={automobile} onChange={handleAutomobileChange} required id="automobile" name="automobile" className="form-select">
                        <option value=''>Choose an automobile</option>
                        {automobiles && automobiles.map(automobile => {
                                return (
                                    <option key={automobile.vin} value={automobile.vin}>
                                        {automobile.vin}
                                    </option>
                                )
                        })}
                    </select>
                </div>
                <div className="mb-3">
                    <select value={salesPerson.number} onChange={handleSalesPersonChange} required id="salesPerson" name="salesPerson" className="form-select">
                        <option value=''>Choose a Sales Person</option>
                        {salesPersons && salesPersons.map(salesPerson => {
                            return (
                                <option key={salesPerson.number} value={salesPerson.number}>
                                    {salesPerson.name}
                                </option>
                        );
                        })}
                    </select>
                </div>
                <div className="mb-3">
                    <select value={customer.id} onChange={handleCustomerChange} required id="customer" name="customer" className="form-select">
                        <option value=''>Choose a customer</option>
                        {customers && customers.map(customer => {
                            return (
                                <option key={customer.id} value={customer.id}>
                                    {customer.name}
                                </option>
                        );
                        })}
                    </select>
                </div>
                <div className="form-floating mb-3">
                    <input value={price} onChange={handlePriceChange} placeholder="Price" required type="number" name="price" id="price" className="form-control" />
                    <label htmlFor="price">Price</label>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
)
}
