import React, { useEffect, useState } from 'react';
import { Link, NavLink } from 'react-router-dom';

function ModelsList(props) {
    const [models, setModels] = useState(props.models);

    const handleDelete = async (modelId) => {
        const response = await fetch(`http://localhost:8100/api/models/${modelId}/`, {
            method: 'DELETE',
        });
        if (response.ok) {
            const updatedModels = models.filter(model => model.id !== parseInt(modelId));
            setModels(updatedModels);
        }
    };


    const getData = async () => {
        const modelsUrl = "http://localhost:8100/api/models/";
        const response = await fetch(modelsUrl);

        if (response.ok) {
            const data = await response.json();
            setModels(data.models)
        }
    }

    useEffect(() => {
        getData()
    }, [])

    return (
        <div className="container my-4">
            <h1 className="text-center mb-4">List of Models</h1>
            <div className="text-center">
                <NavLink to="/models/new" className="btn btn-primary">
                    Create New Model
                </NavLink>
            </div>
            <div className="row">
                {models.map((model) => (
                    <div className="col-md-4 mb-4" key={model.href}>

                        <div className="card">
                            <Link to={`${model.id}`} style={{ textDecoration: 'none', color: 'black' }} key={model.id}>
                                <img
                                    src={model.picture_url}
                                    className="card-img-top"
                                    alt="Model"
                                    style={{ objectFit: "cover" }}
                                />
                            </Link>
                            <div className="card-body text-center">
                                <Link to={`${model.id}`} style={{ textDecoration: 'none', color: 'black' }} key={model.id}>
                                    <p className="card-text"> Model Name: {model.name}</p>
                                    <p className="card-text">Manufacturer: {model.manufacturer.name}</p>
                                </Link>
                                <button
                                    type="button"
                                    className="btn btn-danger"
                                    onClick={() => handleDelete(model.id)}
                                >
                                    Delete
                                </button>
                            </div>
                        </div>

                    </div>
                ))}
            </div>
        </div>
    );
}

export default ModelsList;
