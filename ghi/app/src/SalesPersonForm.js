import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

function SalesPersonForm() {

    const navigate = useNavigate()

    const [formData, setFormData] = useState({
        name: '',
        number: '',
    })

    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = `http://localhost:8090/api/salesPersons/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };



    const response = await fetch(url, fetchConfig);
    if (response.ok) {
        setFormData({
        name: '',
        number: '',
        });
        navigate('/salesPersons')
        }
    }


    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
        ...formData,
        [inputName]: value
        });
    }




    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create a new Sales Person</h1>
            <form onSubmit={handleSubmit} id="create-SalesPerson-form">
                <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.name}placeholder="Ex. Gilbert" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="color">Name</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.number} placeholder="3" required type="number" name="number" id="number" className="form-control" />
                <label htmlFor="vin">Number</label>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
            </div>
        </div>
        </div>
    )
    }
export default SalesPersonForm;
