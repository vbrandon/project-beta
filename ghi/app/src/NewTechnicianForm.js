import React, { useState, useEffect } from 'react';

function NewTechnicianForm(props) {

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {
            name: employeeName,
            employee_number: employeeNumber
        };

        const techUrl = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const techDataResponse = await fetch(techUrl, fetchConfig);
        if (techDataResponse.ok) {
            const newTech = await techDataResponse.json();
            props.onCreate(newTech);

            setName('');
            setEmployeeNumber('');
        } else {
            console.log("*******ERROR. Server response: ", techDataResponse);
        }
    }

    const [employeeName, setName] = useState('');
    const [employeeNumber, setEmployeeNumber] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleEmployeeNumberChange = (event) => {
        const value = event.target.value;
        setEmployeeNumber(value);
    }

    const fetchData = async () => {
        const techniciansListResponse = await fetch('http://localhost:8080/api/technicians/');

        if (techniciansListResponse.ok) {
            const technicianData = await techniciansListResponse.json();
            setTechnicians(technicianData.technician);
        } else {
            console.log("*******ERROR. Server response: ", techniciansListResponse);
        }
    }

    const [technicians, setTechnicians] = useState([]);

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h2>Add A New Technician</h2>
                    <form onSubmit={handleSubmit} id="create-technician-form">
                        <div className="mb-3">
                            <label htmlFor="name">Name</label>
                            <input onChange={handleNameChange} required type="text" name="name" value={employeeName} id="fabric" className="form-control"/>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="employee_number">Employee Id</label>
                            <input onChange={handleEmployeeNumberChange} required type="text" name="employee_number" value={employeeNumber} id="fabric" className="form-control"/>
                        </div>

                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default NewTechnicianForm;