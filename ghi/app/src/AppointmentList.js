import React from "react";

class ListAppointments extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      appointments: []
    };
  }

  async componentDidMount() {
    const url = 'http://localhost:8080/api/appointments';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      this.setState({ appointments: data.appointment });
    }
  }

  handleDelete = async (id) => {
    const url = `http://localhost:8080/api/appointments/${id}`;
    const response = await fetch(url, {
      method: 'DELETE'
    });
    if (response.ok) {
      this.setState((prevState) => ({
        appointments: prevState.appointments.filter(appointment => appointment.id !== id)
      }));
    }
  }

  handleComplete = async (id) => {
    const url = `http://localhost:8080/api/appointments/${id}/`;
    const response = await fetch(url, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({ completed: true })
    });
    
    if (response.ok) {
      window.location.reload();
    }
  }
  
  render() {
    return (
      <div>
        <h1>Appointments</h1>
        <table className="table">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Customer</th>
              <th>Starts</th>
              <th>Reason</th>
              <th>VIP</th>
              <th>Complete/Cancel</th>
            </tr>
          </thead>
          <tbody>
            {this.state.appointments.map((appointment) => {
              if (appointment.completed === false) {
                if (appointment.vip) {
                  return (
                    <tr key={appointment.id}>
                      <td>{appointment.automobile.vin}</td>
                      <td>{appointment.customer_name}</td>
                      <td>{appointment.starts}</td>
                      <td>{appointment.reason}</td>
                      <td>Yes</td>
                      <td>
                        <button onClick={() => this.handleDelete(appointment.id)}>Cancel</button>
                        <button onClick={() => this.handleComplete(appointment.id)}>Complete</button>
                      </td>
                    </tr>
                  );
                } else {
                  return (
                    <tr key={appointment.id}>
                      <td>{appointment.automobile.vin}</td>
                      <td>{appointment.customer_name}</td>
                      <td>{appointment.starts}</td>
                      <td>{appointment.reason}</td>
                      <td>No</td>
                      <td>
                        <button onClick={() => this.handleDelete(appointment.id)}>Cancel</button>
                        <button onClick={() => this.handleComplete(appointment.id)}>Complete</button>
                      </td>
                    </tr>
                  );
                }
              } else {
                return null; // exclude completed appointments from the list
              }
            })}
          </tbody>
        </table>
      </div>
    );
  }
}
export default ListAppointments;