import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

function CustomerForm() {

    const navigate = useNavigate()

    const [formData, setFormData] = useState({
        name: '',
        address: '',
        phone_number: '',
    })

    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = `http://localhost:8090/api/customer/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };



    const response = await fetch(url, fetchConfig);
    if (response.ok) {
        setFormData({
        name: '',
        address: '',
        phone_number: '',
        });
        navigate('/customer')
        }
    }


    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
        ...formData,
        [inputName]: value
        });
    }




    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create a new Customer</h1>
            <form onSubmit={handleSubmit} id="create-customer-form">
                <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.name}placeholder="Ex. Gilbert" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="color">Name</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.address}placeholder="18000 Lincoln St" required type="text" name="address" id="address" className="form-control" />
                <label htmlFor="year">Address</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.phone_number} placeholder="8182123050" required type="number" name="phone_number" id="phone_number" className="form-control" />
                <label htmlFor="vin">Phone number</label>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
            </div>
        </div>
        </div>
    )
    }
export default CustomerForm;
