import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

function AutomobileForm() {

    const navigate = useNavigate()

    const [formData, setFormData] = useState({
        color: '',
        year: '',
        vin: '',
        model_id: '',
    })

    const [models, setModels] = useState([]);

    useEffect(() => {
        const fetchModels = async () => {
            const response = await fetch(`http://localhost:8100/api/models/`);
            if (response.ok) {
                const data = await response.json();
                setModels(data.models);
            }
        };

        fetchModels();
    }, []);


    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = `http://localhost:8100/api/automobiles/`;

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };



    const response = await fetch(url, fetchConfig);
    if (response.ok) {
        setFormData({
        color: '',
        year: '',
        vin: '',
        model_id: '',
        });
        navigate('/automobiles')
        }
    }


    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
        ...formData,
        [inputName]: value
        });
    }




    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create a new Automobile</h1>
            <form onSubmit={handleSubmit} id="create-automobile-form">
                <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.color}placeholder="Ex. Red" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.year}placeholder="Ex.2020" required type="number" name="year" id="year" className="form-control" />
                <label htmlFor="year">Year</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.vin} placeholder="Ex.1C3CC5FB2AN120174" required type="text" name="vin" id="vin" className="form-control" />
                <label htmlFor="vin">vin</label>
                </div>
                <div className="mb-3">
                            <label htmlFor="model" className="form-label">ID:</label>
                            <select onChange={handleFormChange} required id="model_id" name="model_id" value={formData.model_id} className="form-select">
                            <option value=""></option>
                            {models.map(model => {
                                return (
                                    <option key={model.id} value={model.id}>
                                        {model.name}
                                    </option>
                                )
                            })
                            }
                            </select>
                        </div>
                <button className="btn btn-primary">Create</button>
            </form>
            </div>
        </div>
        </div>
    )
    }
export default AutomobileForm;
