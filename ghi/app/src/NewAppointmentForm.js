import React, { useState, useEffect } from 'react';

function NewAppointmentForm(props) {

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {
            automobile: automobile,
            customer_name: customer_name,
            date: date,
            time: time,
            reason: reason,
            technician: technician,

            
        };
        console.log(data)
        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const appointmentDataResponse = await fetch(appointmentUrl, fetchConfig);
        if (appointmentDataResponse.ok) {
            const newAppointment = await appointmentDataResponse.json();
            props.onCreate(newAppointment);

            setCustomerName('');
            setDate('');
            setTimeout('');
            setReason('');
            setAutomobile('');
            setTechnician('')


        } else {
            console.log("*******ERROR. Server response: ", appointmentDataResponse);
        }
    }

    const [technicians, setTechnicians] =useState([]);

    const [customer_name, setCustomerName] = useState('');
    const [date, setDate] = useState('');
    const [time, setTime] = useState('');
    const [reason, setReason] = useState('');
    const [automobile, setAutomobile] = useState('');
    const [technician, setTechnician] = useState('');

    const handleCustomerNameChange = (event) => {
        const value = event.target.value;
        setCustomerName(value);
    }

    const handleDateChange = (event) => {
        const value = event.target.value;
        setDate(value);
    }

    const handleTimeChange = (event) => {
        const value = event.target.value;
        setTime(value);
    }

    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    }

    const handleAutomobileChange = (event) => {
        const value = event.target.value;
        setAutomobile(value);
    }

    const handleTechnicianChange = (event) => {
        const value = parseInt(event.target.value, 10);
        setTechnician(value);
    }

    const fetchData = async () => {
        const techniciansListResponse = await fetch('http://localhost:8080/api/technicians/');

        if (techniciansListResponse.ok) {
            const technicianData = await techniciansListResponse.json();
            setTechnicians(technicianData.technician);
        } else {
            console.log("*******ERROR. Server response: ", techniciansListResponse);
        }
    }


    

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h2>Make A New Appointment</h2>
                    <form onSubmit={handleSubmit} id="create-appointment-form">
                        <div className="mb-3">
                            <label htmlFor="name">Customer Name</label>
                            <input onChange={handleCustomerNameChange} required type="text" name="name" value={customer_name} id="name" className="form-control"/>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="starts">Date</label>
                            <input onChange={handleDateChange} required type="date" name="date" value={date} id="starts" className="form-control"/>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="starts">Start Time</label>
                            <input onChange={handleTimeChange} required type="time" name="time" value={time} id="starts" className="form-control"/>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="reason">Reason</label>
                            <textarea onChange={handleReasonChange} required name="reason" value={reason} id="reason" rows="4" className="form-control"></textarea>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="automobile">VIN</label>
                            <input onChange={handleAutomobileChange} required type="text" name="name" value={automobile} id="vin" className="form-control"/>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="technician">Technician</label>
                            <select onChange={handleTechnicianChange} required id="technician" name="technician" value={technician} className="form-select">
                            <option value=""></option>
                            {technicians.map(technician => {
                                return (
                                    <option key={technician.id} value={technician.id}>
                                        {technician.name}
                                    </option>
                                )
                            })
                            }
                            </select>
                        </div>


                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default NewAppointmentForm;