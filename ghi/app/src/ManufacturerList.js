import React, { useEffect, useState } from 'react';
import { Link, NavLink } from 'react-router-dom';

function ManufacturerList(props) {
    const [manufacturers, setManufacturer] = useState([]);

    const handleDelete = async (manufacturerId) => {
        const response = await fetch(`http://localhost:8100/api/manufacturers/${manufacturerId}/`, {
            method: 'DELETE',
        });
        if (response.ok) {
            const updatedManufacturers = manufacturers.filter(manufacturer => manufacturer.id !== parseInt(manufacturerId));
            setManufacturer(updatedManufacturers);
        }
    };


    const getData = async () => {
        const manufacturersUrl = "http://localhost:8100/api/manufacturers/";
        const response = await fetch(manufacturersUrl);

        if (response.ok) {
            const data = await response.json();
            setManufacturer(data.manufacturers)
        }
    }

    useEffect(() => {
        getData()
    }, [])

    return (
        <div className="container my-4">
            <h1 className="text-center mb-4">List of Manufacturers</h1>
            <div className="text-center">
                <NavLink to="/manufacturers/new" className="btn btn-primary">
                    Create New Manufacturers
                </NavLink>
            </div>
            <div className="row">
                {manufacturers.map((manufacturer) => (
                    <div className="card" key={manufacturer.id}>
                        <div className="card-body text-center">
                            <Link to={`${manufacturer.id}`} style={{ textDecoration: 'none', color: 'black' }}>
                                <p className="card-text"> Name: {manufacturer.name}</p>
                            </Link>
                            <button
                                type="button"
                                className="btn btn-danger"
                                onClick={() => handleDelete(manufacturer.id)}
                            >
                                Delete
                            </button>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
}

export default ManufacturerList;
