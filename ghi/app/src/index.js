import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));

async function loadManufacturers(props) {
  const response = (await fetch('http://localhost:8100/api/manufacturers/'));
  if (response.ok) {
    const data = await response.json();
    root.render (
      <React.StrictMode>
        <App manufacturers ={data.manufacturers} />
      </React.StrictMode>
    );
  }
}

async function loadModel(props) {
  const response = await fetch('http://localhost:8100/api/models/');
  if (response.ok) {
    const data = await response.json();
    root.render(
      <React.StrictMode>
        <App models={data.models}  />
      </React.StrictMode>
    );
  } else {
    console.error(response);
  }
}

async function loadAutomobiles(props) {
  const response = (await fetch('http://localhost:8100/api/automobiles/'));
  if (response.ok) {
    const data = await response.json();
    root.render (
      <React.StrictMode>
        <App automobiles ={data.automobiles} />
      </React.StrictMode>
    );
  }
}



async function loadTechnicians(props) {
  const response = (await fetch('http://localhost:8080/api/technicians/'));
  if (response.ok) {
    const data = await response.json();
    root.render (
      <React.StrictMode>
        <App sale ={data.sale} />
        <App technicians ={data.technician} />
      </React.StrictMode>
    );
  }
}


async function loadAppointment(props) {
  const response = (await fetch('http://localhost:8080/api/appointments/'));
  if (response.ok) {
    const data = await response.json();
    root.render (
      <React.StrictMode>
        <App appointments ={data.appointments} />
      </React.StrictMode>
    );
  }
}

loadManufacturers();
loadModel();
loadAutomobiles();
loadTechnicians();
loadAppointment();
