import { useNavigate } from 'react-router-dom';
import React, {useEffect, useState} from 'react';

function NewModelForm (props) {
    const navigate = useNavigate();
    const handleSubmit = async (event) => {
        event.preventDefault();

       
        const data = {};

        data.name = name;
        data.picture_url = pictureUrl;
        data.manufacturer_id = manufacturer;


        const modelUrl = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const modelDataResponse = await fetch(modelUrl, fetchConfig);
        if (modelDataResponse.ok) {
            const newModel = await modelDataResponse.json();
            props.onCreate(newModel);
   
            setName('');
            setPictureUrl('');
            setManufacturer('');
            
            navigate('/models');
        
        } else {
            console.log("*******ERROR. Server response: ", modelDataResponse);
        }
    }

    const [manufacturers, setManufacturers] = useState([]);

    const [name, setName] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [manufacturer, setManufacturer] = useState('');


    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }
    const handlManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const fetchData = async () => {

        const manufacturerListResponse = await fetch('http://localhost:8100/api/manufacturers/');

        if (manufacturerListResponse.ok) {
            const manfacturerData = await manufacturerListResponse.json();
            setManufacturers(manfacturerData.manufacturers);
        } else {
            console.log("*******ERROR. Server response: ", manufacturerListResponse);
        }

    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h2>Add A New Model</h2>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="mb-3">
                            <label htmlFor="name">Name</label>
                            <input onChange={handleNameChange} required type="text" name="name" value={name} id="fabric" className="form-control"/>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="pictureurl" className="form-label">Picture URL</label>
                            <input onChange={handlePictureUrlChange} type="text" className="form-control" name="pictureurl" value={pictureUrl} id="pictureurl" />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="manufacturer" className="form-label">Manufacturer:</label>
                            <select onChange={handlManufacturerChange} required id="manufacturer" name="manufacturer" value={manufacturer.id} className="form-select">
                            <option value=""></option>
                            {manufacturers.map(manufacturer => {
                                return (
                                    <option key={manufacturer.id} value={manufacturer.id}>
                                        {manufacturer.name}
                                    </option>
                                )
                            })
                            }
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default NewModelForm;
