import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturerList from './ManufacturerList'
import ManufacturerForm from './ManufacturerForm';
import ModelsList from './VehicleModelList';
import NewModelForm from './VehicleModelForm';
import AutomobilesList from './Listautomobiles';
import AutomobileForm from './Automobileform';
import CustomerForm from './CustomerForm';
import SalesPersonForm from './SalesPersonForm';
import ListSales from './ListSales';
import SaleForm from './SaleForm'
import NewTechnicianForm from './NewTechnicianForm';
import NewAppointmentForm from './NewAppointmentForm';
import AppointmentsList from './AppointmentList';
import React, { useEffect, useState } from "react";

function App(props) {
  const [models, setModels] = useState([]);
  const [automobiles, setAutomobiles] = useState([]);
  const [technicians, setTechnicians] = useState([]);
  const [appointments, setAppointments] = useState([]);

  const fetchModels = async () => {
    const response = await fetch('http://localhost:8100/api/models/')
    if (response.ok){
      const data = await response.json();
      setModels(data.models);
    } else {
      console.error(response)
    }
  }

  const fetchAutomobiles = async () => {
    const response = await fetch('http://localhost:8100/api/automobiles/')
    if (response.ok){
      const data = await response.json();
      setAutomobiles(data.automobiles);
    } else {
      console.error(response)
    }
  }

  const fetchTechnicians = async () => {
    const response = await fetch('http://localhost:8080/api/technicians/')
    if (response.ok){
      const data = await response.json();
      setTechnicians(data.technician);
    } else {
      console.error(response)
    }
  }

  const fetchAppointments = async () => {
    const response = await fetch('http://localhost:8080/api/appointments/')
    if (response.ok){
      const data = await response.json();
      setAppointments(data.appointment);
    } else {
      console.error(response)
    }
  }

  useEffect(() => {
    fetchModels();
    fetchAutomobiles();
    fetchTechnicians();
    fetchAppointments();
  }, [])

  const handleCreateModel = (newModel) => {
    setModels([...models, newModel]);
  }

  const handleCreateAutomobile = (newAutomobile) => {
    setAutomobiles([...automobiles, newAutomobile]);
  }

  const handleCreateTechnician = (newTechnician) => {
    setTechnicians([...technicians, newTechnician]);
  }

  const handleCreateAppointment = (newAppointment) => {
    setAppointments([...appointments, newAppointment]);
  }

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers">
            <Route index element={<ManufacturerList manufacturers={props.manufacturers} /> } />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="/models">
            <Route index element={<ModelsList models={models} setModels={setModels} />} />
            <Route path="new" element={<NewModelForm onCreate={handleCreateModel} />} />
          </Route>
          <Route path="automobiles">
            <Route index element={<AutomobilesList automobiles={automobiles} /> } />
            <Route path="new" element={<AutomobileForm onCreate={handleCreateAutomobile} />} />
          </Route>
            <Route path="customer">
            <Route index element={<CustomerForm />} />
          </Route>
          <Route path="salesPersons">
            <Route index element={<SalesPersonForm />} />
          </Route>
          <Route path="sales">
            <Route index element={<ListSales sale={props.sale} /> } />
            <Route path="new" element={<SaleForm />} />
          </Route>
          <Route path="technicians">
            <Route path="new" element={<NewTechnicianForm onCreate={handleCreateTechnician}/>} />
          </Route>
          <Route path="appointments">
            <Route index element={<AppointmentsList appointments={appointments} /> } />
            <Route path="new" element={<NewAppointmentForm onCreate={handleCreateAppointment}/>} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
