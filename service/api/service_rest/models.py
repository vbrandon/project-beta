from django.db import models

# Create your models here.


class AutomobileVO(models.Model):
    color = models.CharField(max_length=100)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=20, unique=True)

class Technicians(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.PositiveSmallIntegerField(unique=True)
    
    
class Appointment(models.Model):
    customer_name = models.CharField(max_length=150)
    starts = models.DateTimeField(null=True)
    reason = models.TextField()
    
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name= "automobile",
        on_delete= models.PROTECT,
    )
    
    technician = models.ForeignKey(
        Technicians,
        related_name= "technician",
        on_delete= models.PROTECT,
    )
    
    completed = models.BooleanField(default=False)
    vip = models.BooleanField(default=False)