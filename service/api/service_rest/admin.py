from django.contrib import admin
from .models import Appointment, Technicians, AutomobileVO
# Register your models here.

admin.site.register(Appointment)
admin.site.register(Technicians)
admin.site.register(AutomobileVO)