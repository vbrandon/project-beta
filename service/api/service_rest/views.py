from django.shortcuts import render
from .models import Appointment, AutomobileVO, Technicians
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
import json
# Create your views here.

class TechnicianEncoder(ModelEncoder):
    model = Technicians
    properties = [
        "id",
        "name",
        "employee_number",
    ]

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "color",
        "year",
    ]

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "automobile",
        "customer_name",
        "starts",
        "reason",
        "technician",
        "vip",
        "completed",
        
    ]
    def get_extra_data(self, o):
        return {"vin": o.automobile.vin}
    
    encoders = {
        "technician" : TechnicianEncoder(),
        "automobile" : AutomobileVOEncoder(),
    }
    


@require_http_methods(["GET", "POST"])
def api_list_techinicians(request):
        if request.method == "GET":
            technician = Technicians.objects.all()
            return JsonResponse(
                {"technician": technician},
                encoder=TechnicianEncoder,
            )
        else:
            try:
                content = json.loads(request.body)
                technician = Technicians.objects.create(**content)
                return JsonResponse(
                    technician,
                    encoder=TechnicianEncoder,
                    safe=False,
                )
                
            except:
                response = JsonResponse(
                    {"message": "Could not create the technician"}
                )
                response.status_code = 400
                return response

@require_http_methods(["GET", "PUT", "DELETE"])
def api_details_techinician(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            technician = Technician.objects.get(id=pk)
            technician.delete()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            technician = Technician.objects.get(id=pk)

            props = ["name"]
            for prop in props:
                if prop in content:
                    setattr(technician, prop, content[prop])
            technician.save()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointment = Appointment.objects.all()
        return JsonResponse(
            {"appointment": appointment},
            encoder=AppointmentEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            vin = content['automobile']
            automobile = AutomobileVO.objects.get(vin=vin)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile"},
                status=400,
            )
        try:
            employee_number = content['technician']
            technician = Technician.objects.get(id=employee_number)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid employee id"},
                status=400,
            )
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_details_appointment(request, pk):

    appointment = Appointment.objects.get(id=pk)
    if request.method == "GET":
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        appointment.delete()
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
    else: 
        content = json.loads(request.body)
        props = ["completed"]
        for prop in props:
            if prop in content:
                setattr(appointment, prop, content[prop])
        appointment.save()
        return JsonResponse(appointment, encoder=AppointmentEncoder, safe=False)


@require_http_methods(["GET"])
def api_show_appointment(request, vin):
    if request.method == "GET":
        automobile = AutomobileVO.objects.get(vin=vin)
        appointment = Appointment.objects.filter(automobile=automobile)
        return JsonResponse(
            {"appointments": appointment},
            encoder= AppointmentEncoder,
            safe=False,
        )