from django.urls import path
from .views import (
    api_details_appointment,
    api_list_appointments,
    api_details_techinician,
    api_list_techinicians,
    api_show_appointment,
)

urlpatterns = [
    path(
        "technicians/",
        api_list_techinicians,
        name= "api_list_techinicians"
    ),
    path(
        "technicians/<int:pk>/",
        api_details_techinician,
        name="api_details_techinician",
    ),
    path(
        "appointments/",
        api_list_appointments,
        name= "api_list_appointments",
    ),
    path(
        "appointments/<int:pk>/",
        api_details_appointment,
        name="api_details_appointment",
    ),
    path(
        "appointments/<str:vin>/",
        api_show_appointment,
        name="api_show_appointment",
    ),
]
