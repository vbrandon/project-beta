from django.shortcuts import render
from .models import AutomobilesVO, Customer, Salesperson, SalesRecord
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
# Create your views here.

class AutoVODetailEncoder(ModelEncoder):
    model = AutomobilesVO
    properties = [
    'year',
    'vin',
    'color',
    "id",
    'sold',
    ]




class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        'name',
        'address',
        'phone_number',
        'id',
    ]


class SalesPersonEncoder(ModelEncoder):
    model= Salesperson
    properties = [
        'name',
        'number',
        'id',
    ]

class SalesEncoder(ModelEncoder):
    model = SalesRecord
    properties = [
        'SalesPerson',
        'customer',
        'automobile',
        'price',
        'id',
    ]

    def get_extra_data(self, o):
        return {}

    encoders = {
    "automobile": AutoVODetailEncoder(),
    "customer": CustomerEncoder(),
    'SalesPerson': SalesPersonEncoder(),

}




@require_http_methods(["GET", "POST"])
def api_SalesPersons(request):
    if request.method == "GET":
        salesPerson = Salesperson.objects.all()
        return JsonResponse(
            {"salesPerson": salesPerson},
            encoder=SalesPersonEncoder,
        )
    else: # POST
        content = json.loads(request.body)
        try:
            salesPerson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesPerson,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create a Sales person"}
            )
            return response

@require_http_methods(["DELETE", "GET",])
def api_SalesPerson(request, id):
    if request.method == "GET":
        try:
            salesPerson = Salesperson.objects.get(id=id)
            return JsonResponse(
                salesPerson,
                encoder=SalesPersonEncoder,
                safe=False
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Sales person does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            salesPerson = Salesperson.objects.get(id=id)
            salesPerson.delete()
            return JsonResponse(
                salesPerson,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Sales person does not exist, cant delete"})







@require_http_methods(["GET", "POST"])
def api_Customers(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        return JsonResponse(
            {"customer": customer},
            encoder=CustomerEncoder,
        )
    else: # POST
        content = json.loads(request.body)
        try:
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create a customer"}
            )
            return response


@require_http_methods(["DELETE", "GET",])
def api_Customer(request, id):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=id)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Customer does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            customer = Customer.objects.get(id=id)
            customer.delete()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Customer does not exist, cant delete"})








@require_http_methods(["GET", "POST"])
def api_Sales(request):
    if request.method == "GET":
        sales = SalesRecord.objects.all()
        fixed_sales = [SalesEncoder().default(sale) for sale in sales]
        return JsonResponse(
            {"sales": fixed_sales},
            safe=False)

    else: # POST
        content = json.loads(request.body)
        try:
            vin = content['automobile']
            automobile = AutomobilesVO.objects.get(vin=vin)
            customer = Customer.objects.get(id=content.get("customer"))
            SalesPerson = Salesperson.objects.get(id=content.get("SalesPerson"))
            sales = SalesRecord.objects.create(
                automobile=automobile,
                customer=customer,
                SalesPerson=SalesPerson,
                price=content.get('price'),
            )
            fixed_sales = SalesEncoder().default(sales)
            return JsonResponse(
                fixed_sales,
                safe=False)
        except (AutomobilesVO.DoesNotExist, Customer.DoesNotExist, Salesperson.DoesNotExist):
            response = JsonResponse(
                {'message': 'Invalid, cannot create sale'},
                status=400
            )
            return response







@require_http_methods(["DELETE", "GET",])
def api_Sale(request, id):
    if request.method == "GET":
        try:
            sale = SalesRecord.objects.get(id=id)
            return JsonResponse(
                sale,
                encoder=SalesEncoder,
                safe=False
            )
        except SalesRecord.DoesNotExist:
            response = JsonResponse({"message": "The sale record does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            sale = SalesRecord.objects.get(id=id)
            sale.delete()
            return JsonResponse(
                sale,
                encoder=SalesEncoder,
                safe=False,
            )
        except SalesRecord.DoesNotExist:
            return JsonResponse({"message": "Sale record does not exist, cant delete"})
