from django.urls import path
from .views import api_SalesPersons, api_SalesPerson, api_Customer, api_Customers, api_Sale, api_Sales


urlpatterns = [
    path("salesPersons/", api_SalesPersons, name="SalesPersons"),
    path("salesPerson/", api_SalesPersons, name="create_salesPerson"),
    path('salesPerson/<int:id>/', api_SalesPerson, name="SalesPerson"),


    path("customers/", api_Customers, name="customers"),
    path("customer/", api_Customers, name="create_customer"),
    path('customer/<int:id>/', api_Customer, name="customer"),


    path("sales/", api_Sales, name="sales"),
    path("sale/", api_Sales, name="create_sale"),
    path('sale/<int:id>/', api_Sale, name="sale")
]
