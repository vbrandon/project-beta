from django.db import models
from django.urls import reverse

# Create your models here.
class Salesperson(models.Model):
    name = models.CharField(max_length=50)
    number = models.PositiveSmallIntegerField()

class Customer(models.Model):
    name = models.CharField(max_length=50)
    address = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=50)

class AutomobilesVO(models.Model):
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=30, unique=True)
    sold = models.BooleanField(default=False)


class SalesRecord(models.Model):
    price = models.CharField(max_length=50)
    SalesPerson = models.ForeignKey(
        Salesperson,
        related_name="Salesperson",
        on_delete=models.PROTECT,
    )
    customer = models.ForeignKey(
        Customer,
        related_name="customer",
        on_delete=models.PROTECT
    )
    automobile = models.ForeignKey(
        AutomobilesVO,
        related_name="automobile",
        on_delete=models.PROTECT
    )

    def get_api_url(self):
        return reverse("sale", kwargs={"id": self.id})


    def __str__(self):
        return self.name
