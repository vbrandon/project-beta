from django.contrib import admin

# Register your models here.
from .models import SalesRecord, Salesperson

@admin.register(SalesRecord)
class SalesRecordAdmin(admin.ModelAdmin):
    pass

@admin.register(Salesperson)
class SalespersonAdmin(admin.ModelAdmin):
    pass
