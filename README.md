# CarCar

Starting app:
1. docker volume create beta-data
2. docker-compose build
3. docker-compose up

Team:

* Matthew Huff - Services Microservice
* Brandon Gomez - Sales Microservice

## Design
CarCar is an app made to manage a dealership and works as a tool to handle employees, customers, inventory, and works as a record of all sales and services. The app is split up into 3 main components: Inventory, Sales, and Services. Both sales and services are integrated with inventory. The sales portion of the app deals with customer sales and keeps track of an employees sell history. The service side deals with technicians who perform services on the cars. The inventory is made to manage the cars that both sales and services use to function and include details such as the model and manufacturer of a car.
## Service microservice

The services microservice oversees the ability to create a new Appointment via the new Appotinment form that pulls from the Inventory Microservice to check and see if the vehicle was sold at the dealership for deals based on their "VIP" status and allows the dealership to assign a technician from their list of technicians on staff. The dealship can also add new technicians when nescessary with the new Technician Form. Fianlly, the user can see the history of service appointments a vehicle has had by searching for that vehicle's VIN. 

There are 4 models included in the microserice:
1. Technicians - Must include fields for: Name and Employee_Number. 

2. Appointment - Customer_name, Starts, Reason, Automobile, Technician, Completed, VIP.

3. AutomobilesVO - Value object that is able to get data from the poller. Includes fields: vin, color, year, and sold.

SERVICES PORT : 8080

## Sales microservice

The sales microservice oversees the ability to create Customers and Sales Persons that then gives the ability to create new sale. The microservice is able to list a full sale history as well as the ability to filter it down to a specific Sales Person history. The poller in the microservices polls data from the Inventory microservice which pulls data for automobiles which is essential in creating a sale.

We have 4 models included in the microservice:

1. Customer - Must include fields for: Customer Name, address,and a phone number.

2. Salesperson - Must inclide fields for: Name and Number

3. AutomobilesVO - Value object that is able to get data from the poller. Includes fields: vin, color, year, and sold.

4. SalesRecord - Price field and also uses fields for an already created Salesperson, Cusotmer, and automobile.


Urls for testing:

List of manufacturers
http://localhost:8100/api/manufacturers/

Create a manufacturer
http://localhost:8100/api/manufacturers/


GET request to api/maufacturers:

{
    "manufacturers": [
    	{
    		"href": "/api/manufacturers/1/",
    		"id": 1,
    		"name": "Chevy"
    	},
    	{
    		"href": "/api/manufacturers/5/",
    		"id": 5,
    		"name": "Honda"
    	}
    ]
}


POST request to api/manufacturers/
Request body:

{
    "name": "BMW"
}

Returns:

{
    "href": "/api/manufacturers/5/",
    "id": 5,
    "name": "BMW"
}



List of vehicle models
http://localhost:8100/api/models/


Create a vehicle model
http://localhost:8100/api/models/


{
    "models": [
	    {
	    	"href": "/api/models/1/",
	    	"id": 1,
	    	"name": "A8",
	    	"picture_url": "https://pictures.dealer.com/b/bernardiaudiaoa/0743/bab41a0972960f05ca972fd6b19d1454x.jpg",
	    	"manufacturer": {
        		"href": "/api/manufacturers/1/",
	    		"id": 1,
	    		"name": "Audi"
	    	}
	    }
    ]
}

POST request to api/models/
Request body:

{
    "name": "M3",
    "picture_url": "https://hips.hearstapps.com/hmg-prod/images/2022-audi-q4-e-tron-102-1664419968.jpg?crop=0.552xw:0.621xh;0.236xw,0.315xh&resize=640:*",
    "manufacturer_id": 5
}

Returns:

{
    "href": "/api/models/2/",
    "id": 2,
    "name": "M3",
    "picture_url": "https://hips.hearstapps.com/hmg-prod/images/2022-audi-q4-e-tron-102-1664419968.jpg?crop=0.552xw:0.621xh;0.236xw,0.315xh&resize=640:*",
    "manufacturer": {
    	"href": "/api/manufacturers/5/",
    	"id": 5,
    	"name": "BMW"
    }
}



List of automobiles
http://localhost:8100/api/automobiles/


Create an automobile
http://localhost:8100/api/automobiles/


GET request to api/automobiles/

Returns:
{
			"href": "/api/automobiles/1C3CC5FB2AN120174/",
			"id": 1,
			"color": "red",
			"year": 2012,
			"vin": "1C3CC5FB2AN120174",
			"model": {
				"href": "/api/models/1/",
				"id": 1,
				"name": "Sebring",
				"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
				"manufacturer": {
					"href": "/api/manufacturers/1/",
					"id": 1,
					"name": "Ford"
				}
			}
		}


POST request to api/automobiles/
{
  "color": "orange",
  "year": 2020,
  "vin": "ABC12345",
  "model_id": 1,
	"sold": true
}


Create a salesperson
http://localhost:8090/api/salesPerson/


POST request to api/salesPerson/

Request body:
{
  "name": "Bob",
  "number": 3
}

Return body:
{
	"name": "Bob",
	"number": 3,
	"id": 7
}


Create a sale
http://localhost:8090/api/sale/


List of all sales
http://localhost:8090/api/sales/



POST request to api/sale/

Request body:
{
    "automobile": "S89374872",
    "customer": 4,
    "SalesPerson": 1,
    "price": 32000
}

Return body:
{
	"href": "/api/sale/9/",
	"SalesPerson": {
		"name": "John",
		"number": 1,
		"id": 1
	},
	"customer": {
		"name": "Derreck",
		"address": "8172 Flight Street",
		"phone_number": "3108219090",
		"id": 4
	},
	"automobile": {
		"year": 2023,
		"vin": "S89374872",
		"color": "Black",
		"id": 3,
		"sold": false
	},
	"price": 32000,
	"id": 9
}

Create a customer
http://localhost:8090/api/customer/

POST request to api/customer/

Request body:
{
  "name": "Derreck",
  "address": "8172 Flight Street",
	"phone_number": "3108219090"
}

Return body:
{
	"name": "Derreck",
	"address": "8172 Flight Street",
	"phone_number": "3108219090",
	"id": 7
}


Create a technician
http://localhost:3000/technicians

POST request to api/technicians/

Request body:
{
	"name": "Sean Myrom",
	"employee_number": "101"
}

{
	"id": 1,
	"name": "Sean Myrom",
	"employee_number": "101"
}



List of service appointments
http://localhost:3000/appointments/

POST request to api/appintments/

Request body:
{
  "vin": "1234567",
  "customer_name": "John Smith",
  "reason": "Oil change",
  "starts": "2023-03-11T09:00:00",
  "technician": 4326

}

{
    "id": 1,
    "automobile": {
        "vin": "123456",
        "color": "silver",
        "year": 2000
    },
    "customer_name": "Jeff",
    "starts": "2023-03-08T11:01:00+00:00",
    "reason": "adsfgh",
    "technician": {
        "id": 4,
        "name": "TEst 2",
        "employee_number": 4326
    },
    "vip": false,
    "completed": false,
    "vin": "123456"
}